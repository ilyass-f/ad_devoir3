/**
 * Created by sidotami on 28/04/17.
 */

package eip;
import jdk.nashorn.internal.ir.debug.JSONWriter;
import org.apache.camel.*;
import  org.apache.camel.builder.RouteBuilder;
import org.apache.camel.http.common.HttpOperationFailedException;
import  org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.model.dataformat.JsonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import  org.apache.log4j.BasicConfigurator;
import org.codehaus.jettison.json.JSONObject;

import java.util.Scanner;



public class ProducerConsumer {

    public static String getMessage(){
        Scanner sc = new Scanner(System.in);
        System.out.println("saisir le message !");
        String str = sc.nextLine();
        if (str.equals("exit")) {System.exit(0);}
        return str ;
    }

    public static void main(String[] args) throws Exception {
        String name="";
        String id="";
        String jsonDATA="";
        String mode="";
        Scanner sc = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);
        System.out.println("Entrez :\n- 1 (Find) \n- 2 (GetALL) \n- 3 (AddAnimal) \n- 4 (DeleteAnimal) \n- 5 (UpdateAnimal) \n- 6 (service Géonames) ");
        mode = sc.nextLine();
        if(mode.equals("DeleteAnimal")|| mode.equals("UpdateAnimal")) {
            System.out.println("Entrez l' ID  !");
            id = sc2.nextLine();
        }
        if(mode.equals("find") || mode .equals("geonames")) {
            System.out.println("Entrez le nom !");
            name = sc2.nextLine();
        }
        if(mode.equals("AddAnimal")|| mode.equals("UpdateAnimal")) {

            System.out.println("Entrez l'objet JSON a ajouter !");
            jsonDATA = sc2.nextLine();
        }


        /****recupérer le message de l'utilisateur****/


      // String str = getMessage();



    // Configure le logger par défaut
        BasicConfigurator.configure();
    // Contexte Camel par défaut
        CamelContext context = new DefaultCamelContext();
    // Crée une route contenant le consommateur
        final String finalName = name;
        final String finalId = id;
        RouteBuilder routeBuilder = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
    // On définit un consommateur 'consumer-1'
                // qui va écrire le message
                from("direct:consumer-1").to("log:affiche-1-log");
                // On définit un consommateur 'consumer-2'
                // qui va écrire le message
                from("direct:consumer-2").to("file:messages");

               from("direct:consumer-all")
                        .choice()
                        .when(header("destinataire").isEqualTo("ecrire"))
                        .to("direct:consumer-2")
                        .otherwise()
                        .to("direct:consumer-1");

                from("direct:REST")
                        .choice()
                        .when(header("mode").isEqualTo("find"))
                        .setHeader(Exchange.HTTP_METHOD,constant("GET"))
                        .to("http://127.0.0.1:8082/animals/findByName/"+finalName)
                        .when(header("mode").isEqualTo("GetALL"))
                        .setHeader(Exchange.HTTP_METHOD,constant("GET"))
                        .to("http://127.0.0.1:8082/animals")
                        .when(header("mode").isEqualTo("DeleteAnimal"))
                        .setHeader(Exchange.HTTP_METHOD,constant("DELETE"))
                        .to("http://127.0.0.1:8082/animals/delete/"+ finalId)
                        .when(header("mode").isEqualTo("geoname"))
                        .setHeader(Exchange.HTTP_METHOD,constant("GET"))
                        .to("http://127.0.0.1:8082/cageposition/"+ finalName)
                        .log("reponse received : ${body}").otherwise().to("direct:RESTwithArgs");


                from("direct:RESTwithArgs")
                        .choice()
                        .when(header("mode").isEqualTo("AddAnimal"))
                        .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                        .setHeader(Exchange.HTTP_METHOD,constant("POST"))
                        .to("http://127.0.0.1:8082/animals/")
                        .when(header("mode").isEqualTo("UpdateAnimal"))
                        .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                        .setHeader(Exchange.HTTP_METHOD,constant("PUT"))
                        .to("http://127.0.0.1:8082/animals/update/"+ finalId)
                        .log("reponse received : ${body}");






            }
        };
// On ajoute la route au contexte
        routeBuilder.addRoutesToCamelContext(context);
// On démarre le contexte pour activer les routes
        context.start();
// On crée un producteur
        ProducerTemplate pt = context.createProducerTemplate();


/*** ajoute l'entête destinataire avec la valeur « écrire » si le message envoyé commence par la lettre« w ».***/

       /* String head = "";
        if (str.startsWith("w")) {
            head = "destinataire";

        }else if (str.equal("exit")) System.exit(0);*/



      // pt.sendBodyAndHeader("direct:consumer-all", str, "destinataire",head);
/***********************************************************************************************************/

/***** Inclustion des services REST ********/

        pt.sendBodyAndHeader("direct:REST",jsonDATA, "mode",mode);




    }
}
